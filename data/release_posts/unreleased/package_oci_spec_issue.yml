features:
  secondary:
    - name: "Non-compliant container images break garbage collection"
      available_in: [core, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/administration/packages/container_registry.html#container-registry-garbage-collection'
      reporter: trizzi
      stage: package
      categories:
        - 'Container Registry'
      issue_url: 'https://gitlab.com/gitlab-org/container-registry/-/issues/404'
      description: |
        According to the [Docker Image Spec](https://docs.docker.com/registry/spec/manifest-v2-2/#manifest-list-field-descriptions) and the [OCI Image Spec](https://github.com/opencontainers/image-spec/blob/master/image-index.md), a manifest list should reference only manifests. However, in [GitLab-#404](https://gitlab.com/gitlab-org/container-registry/-/issues/404), we learned that it's possible to push manifest lists that reference layers. The root cause lies with [Docker's Buildkit](https://docs.docker.com/buildx/working-with-buildx/) (directly or through buildx) implementation of remote cache images. It has been reported [upstream](https://github.com/moby/buildkit/issues/2251), but it isn't clear if this issue will be addressed.

        We have short-term and long-term goals for dealing with this. Short term, our goal is to unblock Self-Managed customers from running offline garbage collection. For now, we updated the container registry to account for this behavior. Long term, we must ensure that the GitLab Container Registry enforces the OCI Image Spec. Allowing manifest lists with blobs to be pushed affects data integrity and consistency. However, adding validation would introduce breaking changes for GitLab.com and Self-Managed customers. We're gathering data on the scope of the issue and will follow up with a plan in the coming months.
        
        In the meantime, if you have any questions, please comment in the [issue](https://gitlab.com/gitlab-org/container-registry/-/issues/407). 
