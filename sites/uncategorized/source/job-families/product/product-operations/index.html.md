---
layout: job_family_page
title: "Product Operations"
description: "GitLab is seeking a Sr. Product Manager to manage the GitLab product development system as a product, and help evolve it into a world class system that can scale rapidly with GitLab the company."
---

## Senior Technical Product Manager, Product Operations

### Role

The mission of the GitLab product team is to consistently create products and experiences that customers love and value. To do this consistently, we strive to create a world class product system in which the extended product team can do the best work of their careers. GitLab is seeking a Senior Technical Product Manager to partner with the Group Manager, Product Operations to manage the GitLab product development system as a product and evolve it into a world class system that can scale rapidly with GitLab the company. The Senior Technical Product Manager will be passionate about the product management discipline and understands the benefits of frameworks/processes and how to optimize them in a way that is effective in delivering value while efficient and as lightweight as possible.

### Responsibilities

_Partner with the Group Manager, Product Operations to:_
- [Operationalize Outcome Driven Products](https://about.gitlab.com/direction/product-operations/#operationalizing-outcome-driven-products)
   - Identify opportunities to automate GitLab product managers' [responsibilities](https://about.gitlab.com/handbook/product/product-manager-responsibilities/#core-pm-responsibilities) and [related tasks](https://about.gitlab.com/handbook/product/product-manager-responsibilities/#core-pm-tasks) to help drive consistency and efficiency across product management as a whole
   - Improve the [release post](url) and [release notes](url) framework for Gitlab the company through process refinements and automation
   - Contribute to the definition and management of the GitLab [product development flow](https://about.gitlab.com/handbook/product-development-flow/)
   - Recommend and implement optimizations to the [product development timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline)
- [Build Qualitative & Quantitive Feedback Loops](https://about.gitlab.com/direction/product-operations/#building-qualitative--quantitative-feedback-loops)
   - Help streamline intelligence as a whole between product management and its stable counterparts within GitLab the company through process/frameworks refinements and automation
   - Leverage automation to boost internal and external knowledge about GitLab's key product communications such as:
     - Updates to product Vision/Strategy, Section Direction and monthly [kick-offs](/handbook/product/product-management/process/#kickoff-meetings)
     - Presentation and discoverability of [release post](url) and [release notes](url)
     - Design and build systems to better share knowledge and align product management and its stable counterparts such as Marketing, Support, Sales and Customer Support
     - Develop tools for collecting and aggregating larger volumes of customer and user feedback from GitLab issues
   - Support maintenance and automation of [Product Operations owned surveys](https://about.gitlab.com/direction/product-operations/#product-operations-survey-results)
   - Help create and maintain Product Operations related intelligence dashboards in Sisense and other tools as needed
- [Scale Product Knowledge](https://about.gitlab.com/direction/product-operations/#scaling-product-knowledge)
   - Help research, organize and recommend resources on product management best practices
   - Contribute content and training [Learning & Development for Product Management](https://about.gitlab.com/handbook/product/product-manager-role/learning-and-development/) to help up-level the skills of the product team
   - Maintain GitLab Learn for Product Management as a [GitLab Learn Curator](https://about.gitlab.com/handbook/people-group/learning-and-development/gitlab-learn/contribute/team-member-contributions/#become-a-gitlab-learn-curator)
   - Constantly improve the [Product Handbook](/handbook/product/) by working collaboratively across product management to update, improve and maintain it
   - Be the DRI of and expert on how to maintain and best leverage GitLab's various templates

### Requirements

- 4+ years of overall experience in either Product Management or related roles
- Experience in customer-first product systems similar to GitLab's, especially important is familiarity with modern customer validation techniques
- Experience supporting the rollout of frameworks/process updates for team of 100+
- Deep knowledge of and demonstrated experience in change management
- Technical skills and familiarity with Git and other tools:
   - Familiarity / Working knowledge with some scripting language (Python, Ruby, Javascript, etc; Ruby preferred)
   - Strong understanding of Git generally and in the context of GitLab (basic commits, branching, merging; rebasing and merge conflict resolution preferred)
   - Can use the GitLab application including issues and merge requests (ability to maintain, debug, and setup pipelines is preferred)
   - Comfortable with using the command line and command line applications (shell, git, homebrew, asdf or other version manager)
   - Comfortable with using a text editor of your choice (e.g., vim, emacs, VS Code, SublimeText, Textmate)
- You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
- You share our [values](/handbook/values/), and work in accordance with those values

## Group Manager, Product Operations

### Role

As the Group Manager, Product Operations, you will drive the vision for GitLab's product system and will be responsible for ensuring the Product Management team's operating health in tight partnership with stable counterparts across GitLab the company. Your "customers" will be a variety of internal constituents, including the VP, Product Management, the CEO, the Product Management team, and other cross-functional departments. This role reports to the Chief Product Officer.

### Individual responsibility

- Develop and drive the vision for a world class product system for GitLab the company and communicate it through [Product Operatoins Direction](https://about.gitlab.com/direction/product-operations)
- Make sure you have a great product operations team (recruit and hire, sense of progress, promote proactively, identify underperformance)
- Collaborate on the group's charter with the VP of Product Management, Chief Product Officer, and CEO; and communicate this charter cross-functionally
- Partner closely on various initiatives across the company with Product Management, Engineering, Design, Marketing, Sales, Customer Success, etc.
- Engineering, design, product team communication / announcements / reinforcing process, policies, and practices
- Develop and maintain a continuing education program for product managers

### Team responsibility

- Serve as "Chief of Staff" for the EVP, Product, attending all key Product Management leadership meetings and helping to ensure execution of Product team decisions
- Help define, document, and ensure consistent execution of the GitLab product manager onboarding / product management / product development process
- Manage a team of 2-4 product managers and/or product operations specialists
- Lead the implementation and drive adoption of large scale process updates for the product team,
- Own communication of product team frameworks/processes to other departments like Marketing, Support, Sales and Customer Success as needed
- Support the [creation of external content](/handbook/product/product-processes/#communication#writing-about-features) including blogs, webinars, and demos

### Requirements

- 7+ years in Product Management
- 5+ years of people management experience
- Past experience in leadership, project management, program management and/or product operations preferred
- Expert in best practices in product management, product design and software development
- Demonstrated success in company-wide alignment and change management for teams of 500+
- Ability to inspire and motivate cross-functional product teams to adopt frameworks that prioritize value to customers/users
- Ability to partner with and align with stakeholders across the business such as marketing, Support, Sales and Customer Success
- Experience in companies that employ modern product management & software development techniques
* You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
* You share our [values](/handbook/values/), and work in accordance with those values
