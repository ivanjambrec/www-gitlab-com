---
layout: handbook-page-toc
title: "TRN.2.01 - Developer Security Training"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# TRN.2.01 - Developer Security Training

## Control Statement

GitLab software engineers are required to complete secure coding training annually.

## Context

The goal is to preserve a baseline of competency and provide continuous learning for the engineering roles that most impact to the product as it moves through stages. Management goals for this function should include maintence and education of secure coding practices as well as integrate lessons learned from historical issues.

## Scope

This control applies to all product engineers and product security PMs.

## Ownership

Control owner:
  * `Security Compliance`

Process owner:
* Training content : SecOps
* Training delivery : People Ops

## Guidance

The delivery process owner is responsible for deploying the training. The control owner checks to ensure 100% of in-scope team members undergo training and reports results to management to assess competency and create goals that are aligned with overall improvement. All product engineers and PMs are responsible for competing the training. Target roles for training includes Frontend Engineers, Backend Engineers and Site Reliability Engineers.

## Additional control information and project tracking

The security training is available in the handbook as optional. Security Compliance is working with the team to clsoe the gaps for the requirements training cadence and management tracking.

For audit evidence of compliance, we need to be able to demonstrate 100% completion of training by all in-scope team members.

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Secure Developer Training control issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/-/issues/933).

### Policy Reference

* Handbook policy [Secure Coding Training](/handbook/engineering/security/secure-coding-training.html)
* [Training feedback form](https://docs.google.com/forms/d/e/1FAIpQLSddXR2VF2iZyOot70ayayofiVuJZM5-8BeBY76tUEJT0oAwYg)

## Framework Mapping

* SOC2 CC
  * CC1.4
