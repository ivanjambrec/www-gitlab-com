---
layout: markdown_page
title: "Procurement - Purchase Order Process"
---

## On this page

{:.no_toc}

- TOC
  {:toc}

The Purchase Order Process isn't live yet but you can read about what it will entail before the go-live date in this page
{: .alert .alert-danger}

## When do I need to create a purchase order?

If your contract has an annual value of $100K or greater, a Purchase Order (PO) must be created to pay your vendor. If an invoice is received that is at or above $100K and there is no PO, the invoice will not be paid until a PO is created which could cause delays to your vendor's receipt of payment.

## Before the Purchase Order is created

It is the responsibility of the business owner/issue creator to request a Purchase Order be generated. To do this, complete the steps below within the vendor contract request issue. Once these steps are completed, a PO will be generated by the procurement team:

1. After the contract has been through the approvals process and signed by **both** parties, attach it to the issue in the appropriate step.
   - A PO will not be created until the signed contract is uploaded to the issue in the appropriate step. The request will be rejected and delayed until the contract is uploaded.
   - A PO will not be created unless all approvals have been received in the contract request issue and your vendor will not be paid.
1. Enter the contract start and end date
   - A PO will not be created until these dates are added to the issue in the appropriate step. The request will be rejected and delayed until this is entered.
1. Add the `PO To-Do` label to the issue. This prompts the PO creation process.
   - A PO will not be created until this label is added to the issue. The request will not be seen and the PO will not be created until this tag is added.
1. Enter the Invoice Approver name. This is the person who will approve and be contacted in the event there are any invoice questions.
   - If no invoice approver name is entered, the PO will default to the person who opened the issue. In the event this is the incorrect person, payment to the vendor may be delayed.

> ### Exceptions to the Purchase Order requirement:

1. Confidential Legal Fees
   - AP will route approvals in Tipalti based on matrix
1. Audit and Tax Fees
   - AP will route approvals in Tipalti based on matrix
1. Benefits & Payroll
   - Includes PEOs and benefit related expenses

- [Mutual Non-Disclosure Agreement](https://drive.google.com/a/gitlab.com/file/d/1hRAMBYrYcd9yG8FOItsfN0XYgdp32ajt/view?usp=sharing)
- [Logo Authorization Template](https://drive.google.com/file/d/1Vtq3UHc8lMfIbVFJ3Mc-PZZjb6_CKAvm/view?usp=sharing)
- [Media Consent and Release Form](https://drive.google.com/file/d/10pplnb9HMK0J0E8kwERi8rRHvAs_rKoH/view?usp=sharing)

## Creation of the Purchase Order

Once a Vendor issue has completed the [Procure to Pay Process](/handbook/finance/procurement), a Purchase Order is created in Netsuite by the procurement team. Before the Purchase Order can be completed, the vendor must self register/setup within Tipalti. Any delay in the Vendor Setup process will delay payment to the Vendor. Refer to [Vendor Master Management](/handbook/finance/accounting/#3-vendor-master-management) for the Vendor set up process.

Detailed Guide with screen shots is available [here](https://docs.google.com/document/d/1iJQHbG1qBFwwis8i_-dtytMP2HY8MNg8rxAH0VS2L3E/edit#).

### Creating a GitLab Standard Purchase Order in Netsuite

This form is used for the following purchase types:

1.  [New Software and/or Existing Software Provider](/handbook/finance/procurement/#1-purchase-type-new-software-andor-existing-software-provider)
2.  [Professional Services and all other contract types](/handbook/finance/procurement/#2-purchase-type-professional-services-and-all-other-contract-types)

Video Guide [here](https://drive.google.com/file/d/1zrcNkAeI-Z1LU4ONUIckcKLsTKkxqeCW/view).

**Steps to create a GitLab Standard PO in NetSuite:**

1.  Login to Netsuite
1.  Under the "Vendors" tab, follow the menu path:
    - Vendors → Purchases → click on “Enter Purchase Orders”.
1.  **Primary Information** - Follow the below guidelines to create the PO correctly:
    - Custom Form → Click on the drop-down, then select "GitLab Standard Purchase Order".
    - Date → This field should auto-populate to the current date.
      - If the date the Purchase Order is being completed is different from the date the contract is signed, update this field to reflect the date the contract is signed/executed.
    - Vendor # → This is an optional field and can be left blank.
    - PO # → "To Be Generated" should be displayed in this field with NO option to update.
    - Vendor → This is a drop down field to select the Vendor, found in step 1 of the finance issue.
      - The Vendor should already be set up prior to creating a new PO. If the Vendor has not been set up, please refer to [Vendor Master Management](/handbook/finance/accounting/#3-vendor-master-management).
    - Memo → Leave it blank.
    - Approval Status → Do not change the option in this box. All approvals should already be obtained and tracked in the Finance Issue.
    - Receive By → Leave it blank.
    - Next Approver → Leave it blank. All approvals should already be obtained and tracked in the Finance Issue.
1.  **Classification** - Follow the below guidelines to create the PO correctly:
    - Subsidiary → This field is auto-populated according to the Vendor selected in the Primary Section.
    - Department → Leave it blank.
    - Class → Leave it blank.
1.  **Intercompany Management** - Follow the below guidelines to create the PO correctly:
    - Paired Intercompany Transaction → Leave it blank.
    - Currency → This field is auto-populated according to the Vendor selected in the Primary Section. Default should be "US Dollar".
    - JE Support: Google Drive Link → Copy and paste the GitLab Finance issue URL into this field.
    - JE Created By → This field is auto-populated with the user that is creating/entering the Purchase Order.
    - Created From → Leave it blank.
    - Estimated Start Date → Date in this field should reflect the date the contract/services will begin. It can be found in the Finance Issue, under step 6.
    - Estimated End Date → Date in this field should reflect the date the contract/services will expire. It can be found in the Finance Issue, under step 6.
    - Billed Amount → Leave it blank. Data in this field will be auto-populated.
    - Amount Remaining → Leave it blank. Data in this field will be auto-populated.
1.  Scroll down to the next section and click on “Items".
    - Click on “items” to add a new line.
      - Item → The GL account found in the Finance Issue, under step 4 and “Budget Approval”.
      - Quantity → The same quantity as the total cost (so an \$100K PO would have the quantity of 100).
      - Units → Leave it blank.
      - Description → Mandatory field where a brief description of the Purchase Order needs to be added. This description is the Title of the issue.
      - Rate → Is always = 1.
      - Amount → Auto-populated after adding the quantity.
      - Options → Leave it blank.
      - Department → The department found in the Finance Issue, under step 4 and “Budget Approval”.
      - Class → Not mandatory for this template. The Campaign Finance Tag, if applicable, is found in the Finance Issue, under step 2.
      - Customer → Leave it blank.
      - Billable → Leave it blank.
      - Related Asset → Leave it blank.
      - Service date → Leave it blank.
      - IC → Leave it blank.
    - Click on "Add".
      - If additional items need to be added, you may add them by manually entering in the new information or selecting "Copy Previous" and updating the fields accordingly.
    - If no additional items need to be added, Click on the "Communication" tab.
    - Select the checkbox "To be E-Mailed" and populate it with the vendor email address.
1.  Open the Custom tab.
    - Requestor name → Person who opened the Finance issue.
    - Requestor’s email → Auto-populated after adding the requestor's name.
1.  Once all information has been entered, select "Save".
1.  The screen will refresh and a message at the top of the screen will be displayed with the transaction save confirmation.

### Creating a Marketing Purchase Order in Netsuite

This form is used for the following purchase type:

1.  [Field Marketing and Events withOUT Confidential Data](/handbook/finance/procurement/#3-purchase-type-field-marketing-and-events-without-confidential-data)

Video Guide [here](https://drive.google.com/file/d/1JMQP94gpOfQbMOddlhdXSKilml4Wz-OH/view).

**Steps to create a Marketing PO in NetSuite**

1. Login to Netsuite
1. Under the "Vendors" tab, follow the menu path:
   - Vendors → Purchases → Enter Purchase Orders.
1. **Primary Information** - Follow the below guidelines to create the PO correctly:
   - Custom Form → Click on the drop-down, then select "Marketing Purchase Order".
   - Date → This field should auto-populate to the current date.
     - If the date the Purchase Order is being completed is different from the date the contract is signed, update this field to reflect the date the contract is signed/executed.
   - Vendor # → This is an optional field and can be left blank.
   - PO # → "To Be Generated" should be displayed in this field with NO option to update.
   - Vendor → This is a drop down field to select the Vendor, found in step 1 of the finance issue.
     - The Vendor should already be set up prior to creating a new PO. If the Vendor has not been set up, please refer to [Vendor Master Management](/handbook/finance/accounting/#3-vendor-master-management).
   - Memo → Leave it blank.
   - Estimated Start Date → Required data entry. Date in this field should reflect the date the contract/services will begin. It can be found in the Finance Issue, under section 1.
   - Estimated End Date → Required data entry. Date in this field should reflect the date the contract/services will expire. It can be found in the Finance Issue, under section 1.
   - JE Support: Google Drive Link → Enter the URL of the GitLab Finance issue.
   - Billed Amount → Data in this field will be auto-populated.
   - Amount Remaining → Data in this field will be auto-populated.
1. **Classification** - Follow the below guidelines to create the PO correctly:
   - Subsidiary →This field is auto-populated according to the Vendor selected in the Primary Section.
   - Department → Leave it blank.
   - Class → Leave it blank.
1. **Intercompany Management** - Follow the below guidelines to create the PO correctly:
   - Paired Intercompany Transaction → Leave it blank.
   - Currency → This field is auto-populated according to the Vendor selected in the Primary Section. Default should be "US Dollar".
   - Requestor Name → The Invoice Approver found in the Finance Issue, under step 5 and “Budget Approval”.
   - Requestor Email → This field should auto-populate after adding a requestor in the Requestor Name field.
1. Scroll down to the next section and click on “Items".
   - Click on “items” to add a new line.
     - Item → The GL account found in the Finance Issue, under step 3 and “Budget Approval”.
     - Quantity → The same quantity as the total cost (so an \$100K PO would have the quantity of 100).
     - Units → Leave it blank.
     - Description → Mandatory field where a brief description of the Purchase Order needs to be added. This description is the Title of the issue.
     - Rate → Is always = 1.
     - Amount → Auto-populated after adding the quantity.
     - Options → Leave it blank.
     - Department → The department found in the Finance Issue, under step 3 and “Budget Approval”.
     - Class → The Campaign Finance Tag found in the Finance Issue (usually under section 1 or in the comments).
     - Customer → Leave it blank.
     - Billable → Leave it blank.
     - Related Asset → Leave it blank.
     - Service date → Leave it blank.
     - IC → Leave it blank.
   - Click on "Add".
1. Once all information has been entered, select "Save".
1. The screen will refresh and a message at the top of the screen will be displayed with the transaction save confirmation.

## Update the Vendor Contract Issue after PO has been created

Once a purchase order has been created and submitted in Netsuite, the below steps have to be followed to complete the process:

1. Add the Purchase Order number to the comments section of the Vendor Contract Issue.
1. Attach the Purchase Order PDF to the comments section of the Vendor Contract Issue.
1. Add the label “PO-Done” to the Vendor Contract Issue.

## PO Change Approval request

If you receive an email notifying that a bill could not be approved due to exceptions during the matching process, start working with the Procurement team by opening a PO Change Approval issue to request the change of an existing Purchase Order. This can include an increase or decrease in the dollar amount or any changes to the currency, product, etc. Procurement will not approve the request if the request is incomplete and/or missing information.

Contact Procurement directly in Slack via #procurement if you have questions.

### Opening a PO Change Approval issue

1.  The Business Owner / Requestor has to open a PO Change Approval issue with [this template](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issuable_template=PO_Change_Approval) to begin the process.
1.  Once the change has been approved by all parties, the Procurement team will amend the Purchase Order in NetSuite.
1.  The Business Owner / Requestor has to send the bill back to Accounts Payable (either by logging into Tipalti or directly clicking on the button "Send back to AP" from the email) and notify that the PO has been amended by adding the PO Change Approval issue URL.
    - **If the bill is not sent back to AP by the Business Owner, the process will not move forward and the invoice won't be paid.**
